<?php 

//Call this function for log the URL 
function _log_url_parameters_log(){
  //Get all the parameters
  $params = _log_url_parameters_get_keys();
  if($params && isset($params['get']) && count($params['get'])) {
    $key_values = _log_url_parameters_get_values($params['get']);
    if($key_values && count($key_values)) {
      $return = array();
      foreach($key_values as $key => $value) {
        $return[] = _log_url_parameters_insert_data_by_key_value($key, $value);
      }
      return $return;
    }
    
  }
  return FALSE;
}

//function to get list of all parameter keys to track
function _log_url_parameters_get_keys(){
  $keys = array();
  $keys['get'] = array(
    'keys',
  );
  return $keys;
}

//Get the values for every key
function _log_url_parameters_get_values($keys) {
  if(!$keys) {
    return FALSE;
  }
  if(!count($keys)) {
    return FALSE;
  }
  $return = array();
  foreach($keys as $key) {
    if(isset($_GET[$key])) {
      $_key = trim($_GET[$key]);
      if($_key) {
        $return[$key] = $_key;
      }
    }
  }
  return $return;
}

function _log_url_parameters_insert_data_by_key_value(&$key, &$value){
  $data = array(
    'key' => $key,
    'value' => $value,
  );
  return _log_url_parameters_insert_data($data);
}

// Function to validate and insert data to table, return the latest id
function _log_url_parameters_insert_data(&$data) {
  // Make sure key is present
  if(!isset($data['key'])) {
    return FALSE;
  }
  if(!trim($data['key'])) {
    return FALSE;
  }  
  // Make sure value is present
  if(!isset($data['value'])) {
    return FALSE;
  }
  if(!trim($data['value'])) {
    return FALSE;
  }  

  //Insert current uid
  if(!isset($data['uid'])) {
    $data['uid'] = \Drupal::currentUser()->id();
  }
  // Put default timestamp
  if(!isset($data['time']) || !intval($data['time'])) {
    $data['time'] = time();
  }

  if(!isset($data['path'])) {
    $data['path'] = '';
  }
  if(!isset($data['extra_data'])) {
    $data['extra_data'] = '';
  }

  $query = db_insert('log_url_parameters_data');
  $query->fields(array('uid', 'key', 'value', 'time', 'path', 'extra_data'));

  $query->values($data);
  $id = $query->execute();
  return $id;
}
