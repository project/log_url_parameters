<?php

function log_url_parameters_views_data() {

  $data = array();
  $data['log_url_parameters_data'] = array();
  $data['log_url_parameters_data']['table'] = array();
  $data['log_url_parameters_data']['table']['group'] = t('URL Parameter logs');
  $data['log_url_parameters_data']['table']['provider'] = 'log_url_parameters_data';
  $data['log_url_parameters_data']['table']['base'] = array(
    'field' => 'lid',
    'title' => t('URL Parameter Log ID'),
    'help' => t('URL Parameter Log ID. Unique ID.'),
    'weight' => -10,
  );

  $data['log_url_parameters_data']['lid'] = array(
    'title' => t('URL Parameter Log ID'),
    'help' => t('Primary key of URL Parameter Log Table.'),
    'field' => array(
      'id' => 'numeric',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'numeric',
    ),
    'argument' => array(
      'id' => 'numeric',
    ),
  );

  $data['log_url_parameters_data']['uid'] = array(
    'title' => t('User ID'),
    'help' => t('User ID of the event.'),
    'field' => array(
      'id' => 'numeric',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'numeric',
    ),
    'argument' => array(
      'id' => 'numeric',
    ),
    'relationship' => array(
      'base' => 'users_field_data',
      'base field' => 'uid',
      'id' => 'standard',
      'label' => t('Users who logged in during the log'),
    ),
  );

  $data['log_url_parameters_data']['key'] = array(
    'title' => t('URL Parameter Key'),
    'help' => t('URL Parameter Key'),
    'field' => array(
      'id' => 'standard',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'string',
    ),
    'argument' => array(
      'id' => 'string',
    ),
  );

  $data['log_url_parameters_data']['value'] = array(
    'title' => t('URL Parameter Value'),
    'help' => t('URL Parameter Key'),
    'field' => array(
      'id' => 'standard',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'string',
    ),
    'argument' => array(
      'id' => 'string',
    ),
  );

  $data['log_url_parameters_data']['time'] = array(
    'title' => t('URL Parameter Log time'),
    'help' => t('URL Parameter Log time.'),
    'field' => array(
      'id' => 'date',
    ),
    'sort' => array(
      'id' => 'date',
    ),
    'filter' => array(
      'id' => 'date',
    ),
  );  


  $data['log_url_parameters_data']['path'] = array(
    'title' => t('URL Parameter Path'),
    'help' => t('URL Parameter Path'),
    'field' => array(
      'id' => 'standard',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'string',
    ),
    'argument' => array(
      'id' => 'string',
    ),
  );
  $data['log_url_parameters_data']['extra_data'] = array(
    'title' => t('URL Parameter Path data'),
    'help' => t('URL Parameter Path Extra data'),
    'field' => array(
      'id' => 'standard',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'string',
    ),
    'argument' => array(
      'id' => 'string',
    ),
  );  
  
  return $data;
}
